import React from "react";

function Footer() {
  return <footer className="bg-dark text-light py-3 fixed-bottom">
      <div className="container">
        <div className="row">
          <div className="col text-center fs-6-">
          © 2024 CARCAR, Inc. All Rights Reserved
          </div>
        </div>
      </div>
    </footer>;
}

export default Footer;
