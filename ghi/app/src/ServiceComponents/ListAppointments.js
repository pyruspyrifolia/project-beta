import React, { useState, useEffect } from "react";

function ListAppointments() {
  const [appointments, setAppointments] = useState([]);

  const handleCancel = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${id}/cancel/`,
        {
          method: "PUT"
        }
      );
      if (response.ok) {
        setAppointments(prevAppointments =>
          prevAppointments.filter(appointment => appointment.id !== id)
        );
      } else {
        console.error("Failed to cancel appointment:", response.statusText);
      }
    } catch (error) {
      console.error("Error canceling appointment:", error);
    }
  };

  const handleFinish = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${id}/finish/`,
        {
          method: "PUT"
        }
      );
      if (response.ok) {
        setAppointments(prevAppointments =>
          prevAppointments.filter(appointment => appointment.id !== id)
        );
      } else {
        console.error("Failed to finish appointment:", response.statusText);
      }
    } catch (error) {
      console.error("Error finishing appointment:", error);
    }
  };

  const fetchAppointments = async () => {
    try {
      const response = await fetch("http://localhost:8080/api/appointments/");
      if (response.ok) {
        const data = await response.json();
        const filteredAppointments = data.appointments.filter(
          appointment =>
            appointment.status !== "canceled" &&
            appointment.status !== "finished"
        );
        setAppointments(filteredAppointments);
      } else {
        console.error("Failed to fetch appointments:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching appointments:", error);
    }
  };

  useEffect(() => {
    fetchAppointments();
  }, []);

  function formatDate(dateString) {
    const options = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      timeZone: "UTC"
    };
    return new Date(dateString).toLocaleDateString(undefined, options);
  }

  function formatTime(timeString) {
    const options = { hour: "2-digit", minute: "2-digit", timeZone: "UTC" };
    return new Date(`1970-01-01T${timeString}Z`).toLocaleTimeString(
      undefined,
      options
    );
  }

  return (
    <div className="container mt-4">
      <h2>Service Appointments</h2>

      <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">VIN</th>
            <th scope="col">Is VIP?</th>
            <th scope="col">Customer</th>
            <th scope="col">Date</th>
            <th scope="col">Time</th>
            <th scope="col">Technician</th>
            <th scope="col">Reason</th>
            <th scope="col">Cancel</th>
            <th scope="col">Finish</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map(appointment =>
            <tr key={appointment.id}>
              <td>
                {appointment.vin}
              </td>
              <td>
                {appointment.vip ? "True" : "False"}
              </td>
              <td>
                {appointment.customer_name}
              </td>
              <td>
                {formatDate(appointment.date)}
              </td>
              <td>
                {formatTime(appointment.time)}
              </td>
              <td>{`${appointment.technician.first_name} ${appointment
                .technician.last_name}`}</td>
              <td>
                {appointment.reason}
              </td>
              <td>
                {appointment.status !== "finished" &&
                  <button
                    className="btn btn-danger me-2"
                    onClick={() => handleCancel(appointment.id)}
                  >
                    Cancel
                  </button>}
              </td>
              <td>
                {appointment.status !== "finished" &&
                  <button
                    className="btn btn-success"
                    onClick={() => handleFinish(appointment.id)}
                  >
                    Finish
                  </button>}
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}

export default ListAppointments;
