import React, { useState, useEffect } from "react";

function ServiceHistory() {
  const [searchVin, setSearchVin] = useState("");
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  useEffect(() => {
    const fetchAppointments = async () => {
      const response = await fetch("http://localhost:8080/api/appointments/");
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
        setFilteredAppointments(data.appointments);
      }
    };

    fetchAppointments();
  }, []);

  const handleSearch = event => {
    event.preventDefault();
    const filtered = appointments.filter(appointment =>
      appointment.vin.toLowerCase().includes(searchVin.toLowerCase())
    );
    setFilteredAppointments(filtered);
  };

  function formatDate(dateString) {
    const options = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      timeZone: "UTC"
    };
    return new Date(dateString).toLocaleDateString(undefined, options);
  }

  function formatTime(timeString) {
    const options = { hour: "2-digit", minute: "2-digit", timeZone: "UTC" };
    return new Date(`1970-01-01T${timeString}Z`).toLocaleTimeString(
      undefined,
      options
    );
  }


  filteredAppointments.sort((a, b) => {
    const dateTimeA = new Date(`${a.date}T${a.time}Z`);
    const dateTimeB = new Date(`${b.date}T${b.time}Z`);
    return dateTimeA - dateTimeB;
  });

return (
  <div className="container mt-4">
    <h2>Service History</h2>
    <form onSubmit={handleSearch}>
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Search by VIN..."
          value={searchVin}
          onChange={e => setSearchVin(e.target.value)}
        />
        <div className="input-group-append">
          <button className="btn btn-primary" type="submit">
            Search
          </button>
          <button
            className="btn btn-secondary"
            type="button"
            onClick={() => {
              setSearchVin("");
              setFilteredAppointments(appointments);
            }}
          >
            Clear
          </button>
        </div>
      </div>
    </form>
    <table className="table">
      <thead>
        <tr>
          <th scope="col">VIN</th>
          <th scope="col">Is VIP?</th>
          <th scope="col">Customer</th>
          <th scope="col">Date</th>
          <th scope="col">Time</th>
          <th scope="col">Technician</th>
          <th scope="col">Reason</th>
          <th scope="col">Status</th>
        </tr>
      </thead>
      <tbody>
        {filteredAppointments.map(appointment =>
          <tr key={appointment.id}>
            <td>
              {appointment.vin}
            </td>
            <td>
              {appointment.vip ? "True" : "False"}
            </td>
            <td>
              {appointment.customer_name}
            </td>
            <td>
              {formatDate(appointment.date)}
            </td>
            <td>
              {formatTime(appointment.time)}
            </td>
            <td>{`${appointment.technician.first_name} ${appointment.technician
              .last_name}`}</td>
            <td>
              {appointment.reason}
            </td>
            <td>
              {appointment.status}
            </td>
          </tr>
        )}
      </tbody>
    </table>
  </div>
);
}

export default ServiceHistory;
