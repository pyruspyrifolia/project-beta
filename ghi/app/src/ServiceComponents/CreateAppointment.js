import React, { useState, useEffect } from "react";

function CreateAppointment() {
  const initialState = {
    vin: "",
    customer_name: "",
    date: "",
    time: "",
    technician: "",
    reason: ""
  };

  const [formData, setFormData] = useState(initialState);
  const [technicians, setTechnicians] = useState([]);
  const [showToast, setShowToast] = useState(false);
  const [toastMessage, setToastMessage] = useState("");

  useEffect(() => {
    const fetchTechnicians = async () => {
      try {
        const response = await fetch("http://localhost:8080/api/technicians/");
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        const data = await response.json();
        setTechnicians(data.technicians);
      } catch (error) {
        console.error(
          "There has been a problem with your fetch operation:",
          error
        );
      }
    };

    fetchTechnicians();
  }, []);

  const handleSubmit = async event => {
    event.preventDefault();
    const appointmentData = {
      ...formData,
      date: formData.date,
      time: formData.time
    };

    try {
      const response = await fetch("http://localhost:8080/api/appointments/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(appointmentData)
      });
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      setFormData(initialState);
      setToastMessage("Appointment Created!");
      setShowToast(true);
    } catch (error) {
      console.error("Failed to submit form:", error);
      setToastMessage("Failed to submit form");
      setShowToast(true);
    }
  };

  const handleInputChange = event => {
    const { name, value } = event.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  return (
    <div className="container mt-5">
      <h1>Create a Service Appointment</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="vin" className="form-label">
            Automobile VIN
          </label>
          <input
            type="text"
            className="form-control"
            id="vin"
            name="vin"
            value={formData.vin}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="customer_name" className="form-label">
            Customer Name
          </label>
          <input
            type="text"
            className="form-control"
            id="customer_name"
            name="customer_name"
            value={formData.customer_name}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="date" className="form-label">
            Date
          </label>
          <input
            type="date"
            className="form-control"
            id="date"
            name="date"
            value={formData.date}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="time" className="form-label">
            Time
          </label>
          <input
            type="time"
            className="form-control"
            id="time"
            name="time"
            value={formData.time}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="technician" className="form-label">
            Technician
          </label>
          <select
            className="form-select"
            id="technician"
            name="technician"
            value={formData.technician}
            onChange={handleInputChange}
            required
          >
            <option value="">Choose a technician...</option>
            {technicians.map(tech =>
              <option key={tech.employee_id} value={tech.employee_id}>
                {tech.first_name} {tech.last_name}
              </option>
            )}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="reason" className="form-label">
            Reason
          </label>
          <textarea
            className="form-control"
            id="reason"
            name="reason"
            value={formData.reason}
            onChange={handleInputChange}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Create Appointment
        </button>
      </form>

      {showToast &&
        <div
          className="toast-container position-fixed top-50 start-50 translate-middle"
          style={{ zIndex: 1050 }}
        >
          <div
            className="toast show"
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
          >
            <div className="toast-header bg-dark text-white">
              <strong className="me-auto">Notification</strong>
              <button
                type="button"
                className="btn-close btn-close-white"
                data-bs-dismiss="toast"
                aria-label="Close"
                onClick={() => setShowToast(false)}
              />
            </div>
            <div className="toast-body bg-white text-dark">
              {toastMessage}
              <button
                type="button"
                className="btn btn-primary mt-2"
                onClick={() => (window.location.href = "/appointments")}
              >
                See All Appointments
              </button>
            </div>
          </div>
        </div>}
    </div>
  );
}

export default CreateAppointment;
