import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Nav from "./Nav";
import MainPage from "./MainPage";
import ListAppointments from "./ServiceComponents/ListAppointments";
import ListTechnicians from "./ServiceComponents/ListTechnicians";
import AppointmentForm from "./ServiceComponents/CreateAppointment";
import AddTechnician from "./ServiceComponents/AddTechnician";
import ServiceHistory from "./ServiceComponents/ServiceHistory";
import AddManufacturer from "./InventoryComponents/Add_Manufacturer";
import ListVehicleModels from "./InventoryComponents/List_VehicleModels";
import ListManufacturers from "./InventoryComponents/List_Manufacturers";
import Footer from "./Footer";
import NewSalespersonForm from './Sales/NewSalesperson';
import NewCustomerForm from './Sales/NewCustomer';
import NewSaleForm from './Sales/NewSale';
import ListSalespeople from './Sales/ListSalesperson';
import ListCustomers from './Sales/ListCustomers';
import ListSales from './Sales/ListSales';
import SalespersonHistory from './Sales/SalespersonHistory';
import NewVehicleModelForm from "./InventoryComponents/Create_VehicleModel";
import ListAutomobiles from "./InventoryComponents/List_Automobiles";
import NewAutomobileForm from "./InventoryComponents/Create_Automobile";

function App() {
  return (
    <Router>
      <div>
        <Nav />
        <main>
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="/appointments" element={<ListAppointments />} />
            <Route path="/technicians" element={<ListTechnicians />} />
            <Route path="/manufacturers" element={<ListManufacturers />} />
            <Route path="/manufacturers/new" element={<AddManufacturer />} />
            <Route path="/appointments/new" element={<AppointmentForm />} />
            <Route path="/technicians/new" element={<AddTechnician />} />
            <Route path="/service-history" element={<ServiceHistory />} />
            <Route path="/vehicle-models" element={<ListVehicleModels />} />
            <Route path="/vehicle-models/new" element={<NewVehicleModelForm />} />
            <Route path="/automobiles" element={<ListAutomobiles />} />
            <Route path="/automobiles/new" element={<NewAutomobileForm />} />
            <Route path="salespeople">
              <Route index element={<ListSalespeople />} />
              <Route path="new" element={<NewSalespersonForm />} />
              <Route path="history" element={<SalespersonHistory />} />
            </Route>
            <Route path="customer">
              <Route index element={<ListCustomers />} />
              <Route path="new" element={<NewCustomerForm />} />
            </Route>
            <Route path="sale">
              <Route index element={<ListSales />} />
              <Route path="new" element={<NewSaleForm />} />
            </Route>
        </Routes>
        </main>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
