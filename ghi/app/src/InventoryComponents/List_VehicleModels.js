import React, { useEffect, useState } from "react";

function ListVehicleModels() {
  const [vehicleModels, setVehicleModels] = useState([]);


  useEffect(() => {
    const fetchVehicleModels = async () => {
      try {
        const response = await fetch(
          "http://localhost:8100/api/models/"
        );
        if (!response.ok) throw new Error("Failed to fetch vehicle models");
        const data = await response.json();
        setVehicleModels(data.models);
      } catch (error) {
        console.error(error.message);
      }
    };
    fetchVehicleModels();
  }, []);

  return (
    <div className="container mt-4">
      <h2 className="card-title display-4 custom-color">Vehicle Models</h2>
      <div className="row">
        {vehicleModels.map(model =>
          <div key={model.id} className="col mb-4">
            <div className="card shadow">
              <img
                src={model.picture_url}
                className="card-img-top"
                alt={model.name}
              />
              <div className="card-body">
                <h5 className="card-title">
                  {model.name}
                </h5>
                <p className="card-text">
                  Manufacturer: {model.manufacturer.name}
                </p>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default ListVehicleModels;
