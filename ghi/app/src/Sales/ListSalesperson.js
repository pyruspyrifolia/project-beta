import React, { useEffect, useState } from "react";
import NewSalespersonForm from "./../Sales/NewSalesperson";

const ListSalespeople = () => {
  const [salespeople, setSalespeople] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchSalespeople = async () => {
      try {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (!response.ok) throw new Error("Failed to fetch salespeople");
        const data = await response.json();
        setSalespeople(data.salesperson);
        setLoading(false);
      } catch (error) {
        console.error(error.message);
      }
    };
    fetchSalespeople();
  }, []);

  return (
    <div className="container mt-4">
      <div className="text-center">
        <h2> SALES TEAM</h2>
        {loading
          ? <p>Loading, one moment please</p>
          : salespeople.length === 0
            ? <div>
                <h5>No salespersons in history</h5>
                <NewSalespersonForm />{" "}
              </div>
            : <table className="table table-center">
                <thead>
                  <tr>
                    <th scope="col">Employee ID</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                  </tr>
                </thead>
                <tbody>
                  {salespeople.map(salesperson =>
                    <tr key={salesperson.id}>
                      <td>
                        {salesperson.employee_id}
                      </td>
                      <td>
                        {salesperson.first_name}
                      </td>
                      <td>
                        {salesperson.last_name}
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>}
      </div>
    </div>
  );
};

export default ListSalespeople;
