"""sales_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from sales_rest.views import (api_customer, api_list_customer, api_list_sale,
                              api_list_salesperson, api_sale, api_salesperson)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/salespeople/", api_list_salesperson, name="list/make salespeople"),
    path(
        "api/salespeople/<int:pk>/",
        api_salesperson,
        name="Details/Update/Delete salesperson",
    ),
    path("api/customers/", api_list_customer, name="list/make customer"),
    path(
        "api/customers/<int:pk>/", api_customer, name="Details/Update/Delete Customer"
    ),
    path("api/sales/", api_list_sale, name="list/make sale"),
    path("api/sale/<int:pk>/", api_sale, name="Details/Update/Delete Sale"),
]
