import json

from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .encoders import (AppointmentDetailEncoder, AppointmentListEncoder,
                       TechnicianListEncoder)
from .models import Appointment, AutomobileVO, Technician


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians}, encoder=TechnicianListEncoder, safe=False
        )
    if request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False)
    return JsonResponse({"message": "Method not allowed"}, status=405)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentListEncoder
        )
    content = json.loads(request.body)
    technician = Technician.objects.get(employee_id=content["technician"])
    content["technician"] = technician
    try:
        automobile = AutomobileVO.objects.get(vin=content["vin"])
        content["vip"] = automobile.sold
    except AutomobileVO.DoesNotExist:
        content["vip"] = False
    content["status"] = "created"
    appointment = Appointment.objects.create(**content)
    return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)

    content = json.loads(request.body)
    try:
        if "technician" in content:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
    except Technician.DoesNotExist:
        return JsonResponse({"message": "Technician doesn't exist"})

    appointment = Appointment.objects.get(pk=pk)
    try:
        automobile = AutomobileVO.objects.get(vin=appointment.vin)
        appointment.vip = automobile.sold
    except AutomobileVO.DoesNotExist:
        appointment.vip = False

    appointment.update(**content)
    appointment.save()

    return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)


@require_http_methods(["PUT"])
def api_status_cancel(request, pk):
    try:
        appointment = Appointment.objects.get(pk=pk)
        appointment.status = Appointment.Status.CANCELED
        appointment.save()
        return JsonResponse(
            {"message": "Appointment canceled successfully"}, status=200
        )
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment does not exist"}, status=404)


@require_http_methods(["PUT"])
def api_status_finish(request, pk):
    appointment = Appointment.objects.get(pk=pk)
    appointment.status = Appointment.Status.FINISHED
    appointment.save()

    return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)
