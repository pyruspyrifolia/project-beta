from django.contrib import admin

from .models import Appointment, AutomobileVO, Technician


@admin.register(Technician)
class TechnicianAdmin(admin.ModelAdmin):
    list_display = ["first_name", "last_name", "employee_id"]


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ["vin", "import_href"]


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = [
        "customer_name",
        "vip",
        "vin",
        "date",
        "time",
        "technician",
        "reason",
        "status",
    ]
