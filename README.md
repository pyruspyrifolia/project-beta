# Project CarCar

Team:

* Sean is working on Automobile Sales API
* Stephany is working on Automobile Service API

## Design
![CARCAR](CarCar.png)

## How to Get Started
Fork the repository

Run the following commands on your computer
- docker volume create beta-data
- docker-compose build
- docker-compose up

## Automobile Sales microservice
In the Automobile Sales microservice, we interact with the following models:

1. **Salesperson**: Represents a salesperson responsible for selling automobiles.
   - Fields:
     - `first_name`: CharField (max_length=200) - First name of the Salesperson.
     - `last_name`: CharField (max_length=200) - Last name of the Salesperson.
     - `employee_id`: CharField (max_length=100) - Employee ID of the Salesperson.

2. **Customer**: Represents a customer buying a vehicle.
   - Fields:
     - `first_name`: CharField (max_length=200) - First name of the Customer.
     - `last_name`: CharField (max_length=200) - Last name of the Customer.
     - `phone_number`: CharField (max_length=200) - Phone Number of the Customer.
     - `address`: CharField (max_length=200) - Address of the Customer.

3. **Sale**: Represents an appointment for automobile service.
	 - Fields:
     - `automobile`: ForeignKey( AutomobileVO, related_name="automobile", on_delete=models.CASCADE) - Automobile that is being bought.
     - `salesperson`: ForeignKey(Salesperson, related_name="salesperson", on_delete=models.CASCADE) - Salesperson selling the Automobile.
     - `customer`: ForeignKey(Customer, related_name="customer", on_delete=models.CASCADE) - Customer buying the Automobile.
     - `price`: CharField (max_length=200) - Price of the automobile.

4. **AutomobileVO**: Represents an Automobile Value Object from the Inventory Database.
	 - Fields:
     - `import_href`: CharField (max_length=200, unique=True) - HREF of the automobile.
     - `vin`: CharField (max_length=17, unique=True) - VIN number of the automobile.
     - `sold`: BooleanField (default=false) - Status of vehicle's sold state.

#### Integration with Inventory Microservice

The Automobile Sales microservice integrates with the Inventory microservice in the following ways:
- Sales When creating a sale, we check if the automobile's status of being "sold" is false in order for it to be listed for a sale. Once the sale is finished, data is sent back to the Inventory API to update a vehicle's sold status as True

#### Bounded Contexts and Value Objects

- **Bounded Contexts**:
In the Automobile Sales microservice, the bounded context of "Sales" has been defined to specifically handle all sales related to automobiles. This bounded context ensures that all functionalities related to sales are grouped together, allowing for better organization and separation of concerns within the microservice architecture. By isolating sale-related logic within this context, it becomes easier to maintain and extend the service without affecting other parts of the system.

- **Value Objects**: Within the bounded context of Sales, the Salesperson and Customer objects serve as value objects representing key concepts in the domain. Salespersons encapsulate information about individuals responsible for selling automobiles, including their names and employee IDs. Customers capture details about individuals that are buying automobiles, such as the customer's name, their address, and phone number. By modeling these entities as value objects, we ensure that they are immutable and self-contained, facilitating clearer understanding and management of appointment-related operations within the service microservice.
Additionally, the AutomobileVO object represents an Automobile Value Object retrieved from the Inventory microservice, containing relevant information about automobiles in the system.

## Automobile Service microservice

#### Models

In the Automobile Service microservice, we interact with the following models:

1. **Technician**: Represents a technician responsible for servicing automobiles.
   - Fields:
     - `first_name`: CharField (max_length=100, null=True, default="Unknown") - First name of the technician.
     - `last_name`: CharField (max_length=100, null=True, default="Unknown") - Last name of the technician.
     - `employee_id`: CharField (max_length=100, null=True, default="Unknown") - Employee ID of the technician.

2. **Appointment**: Represents an appointment for automobile service.
   - Fields:
     - `customer_name`: CharField (max_length=200, default="Default Customer") - Name of the customer.
     - `vin`: CharField (max_length=300) - Vehicle Identification Number (VIN) of the automobile.
     - `vip`: BooleanField (default=False) - Indicates if the customer is a VIP.
     - `date`: DateField (null=True) - Date of the appointment.
     - `time`: TimeField (null=True) - Time of the appointment.
     - `technician`: ForeignKey to Technician - Technician assigned to the appointment.
     - `reason`: TextField - Reason for the appointment.
     - `status`: CharField (max_length=10, choices=Status.choices, default=Status.SCHEDULED) - Status of the appointment.

#### Integration with Inventory Microservice

The Automobile Service microservice integrates with the Inventory microservice in the following ways:
- Appointment Integration When creating or updating appointments, we check if the automobile's VIN exists in the inventory. If it does, we update the appointment's `vip` field based on the automobile's availability in the inventory.

#### Bounded Contexts and Value Objects

- **Bounded Contexts**:
In the Automobile Service microservice, the bounded context of "Service Appointment Management" has been defined to specifically handle the lifecycle of appointments for automobile servicing. This bounded context ensures that all functionalities related to managing service appointments are grouped together, allowing for better organization and separation of concerns within the microservice architecture. By isolating appointment-related logic within this context, it becomes easier to maintain and extend the service without affecting other parts of the system.

- **Value Objects**: Within the bounded context of Service Appointment Management, the Technician and Appointment objects serve as value objects representing key concepts in the domain. Technicians encapsulate information about individuals responsible for servicing automobiles, including their names and employee IDs. Appointments capture details about scheduled service appointments, such as customer information, appointment date and time, assigned technicians, and appointment status. By modeling these entities as value objects, we ensure that they are immutable and self-contained, facilitating clearer understanding and management of appointment-related operations within the service microservice.
Additionally, the AutomobileVO object represents an Automobile Value Object retrieved from the Inventory microservice, containing relevant information about automobiles in the system.



#### CRUD Routes

For CRUD operations on automobile-related resources, please refer to the [Automobile Service AP](https://codingworld.notion.site/Automobile-Service-API-0bc3f9fca9104b7b919b5b54c46e5693)I Documentation provided below.

## API Documentation

- [Inventory API](https://codingworld.notion.site/Inventory-API-Documentation-44d0eea54ebe44edbd20e08590a1cfc6?pvs=74)
- [Automobile API](https://codingworld.notion.site/Automobile-Service-API-0bc3f9fca9104b7b919b5b54c46e5693)
- [Automobile Sales API](https://mire-tower-c35.notion.site/Automobile-Sales-API-0c2ac12677f64ed696f34a6b1017c448)
